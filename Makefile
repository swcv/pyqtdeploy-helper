
QT5SRC=qt5-src
QT5VER=5.12.2
SSLVER=openssl-1.1.1d

default: help

help:
	@echo "Make targets in chronological order:"
	@echo " download_source_packages:  Download dependency packages required by the demo app"
	@echo " clone_qt5_source:          Clone Qt source code"
	@echo " build_android_ssl:         Doownload and build android OpenSSL"
	@echo " build_qt5_with_ssl:        Build Qt from source using OpenSSL"
	@echo " virtualenv:                Setup suitable virtualenv for pyqtdeploy"
	@echo " download_demo_app:         Download pyqtdepoy demo application"
	@echo " build_demo_app:            Build the demo application (linux-x86_64)"
	@echo " build_demo_app_android:    Build the demo application (android armv7)"
	@echo " clean:                     Clean everything away"

download_source_packages:
	make -C source_archive

clone_qt5_source:
	git clone git://code.qt.io/qt/qt5.git $(QT5SRC)
	cd $(QT5SRC) && git checkout $(QT5VER) && git submodule update --init --recursive

build_android_ssl:
	if [ ! -f ./$(QT5SRC)/$(SSLVER)/SSL_DONE ]; then \
	 set -x; \
	 cd $(QT5SRC); \
	 wget https://www.openssl.org/source/$(SSLVER).tar.gz; \
	 tar xvf $(SSLVER).tar.gz; \
	 cd $(SSLVER); \
	 PATH=$(ANDROID_NDK_HOME)/toolchains/llvm/prebuilt/linux-x86_64/bin:$(PATH) ./Configure shared android-arm -D__ANDROID_API__=21 --prefix=$(HOME)/Android/Qt-$(QT5VER)/android_armv7; \
	 PATH=$(ANDROID_NDK_HOME)/toolchains/llvm/prebuilt/linux-x86_64/bin:$(PATH) make depend; \
	 PATH=$(ANDROID_NDK_HOME)/toolchains/llvm/prebuilt/linux-x86_64/bin:$(PATH) make -j 4 SHLIB_VERSION_NUMBER= SHLIB_EXT=_1_1.so; \
	 PATH=$(ANDROID_NDK_HOME)/toolchains/llvm/prebuilt/linux-x86_64/bin:$(PATH) make install SHLIB_VERSION_NUMBER= SHLIB_EXT=_1_1.so; \
	 touch SSL_DONE; \
	fi

build_qt5_with_ssl: build_android_ssl
	set -x; \
	cd $(QT5SRC); \
	./configure -xplatform android-clang --disable-rpath -nomake tests -nomake examples -android-ndk $(HOME)/Android/ndk -android-sdk $(HOME)/Android/sdk -no-warnings-are-errors --prefix=$(HOME)/Android/Qt-$(QT5VER)/android_armv7 -I $(HOME)/Android/Qt-$(QT5VER)/android_armv7/include -L $(HOME)/Android/Qt-$(QT5VER)/androd_armv7/lib; \
	make -j 4; \
	make -j 4 install

virtualenv:
	#
	# pre-build virtualenv with python3.6
	#
	bash -c "if [ ! -d env ]; then \
	  python3 -m venv env; \
	  source env/bin/activate; \
	  pip3 install -r requirements.txt; \
	fi;"
	@echo "Virtual env is installed!"
	@echo "Now, activate it MANUALLY by:"
	@echo " $ source env/bin/activate"

download_demo_app: PKG=pyqtdeploy-2.4.tar.gz
download_demo_app:
	@if [ ! -f "${PKG}" ]; then \
		wget https://files.pythonhosted.org/packages/f2/50/f40f5c453e869d2c7da10ddc8b291da0d2815ccaa4390cb49ad2c801091a/pyqtdeploy-2.4.tar.gz; \
	fi;
	tar xf ${PKG}
	@echo "pyqtdeploy demo app downloaded!"

build_demo_app: download_source_packages
	cd pyqtdeploy-2.4/demo && \
	python3 build-demo.py --verbose --source-dir=../../source_archive

build_demo_app_android: download_source_packages
	cd pyqtdeploy-2.4/demo && \
	ANDROID_NDK_ROOT=$(HOME)/Android/ndk ANDROID_NDK_PLATFORM=android-24 ANDROID_SDK_ROOT=$(HOME)/Android/sdk python3 build-demo.py --verbose --target android --installed-qt-dir $(HOME)/Android/Qt-$(QT5VER)/ --source-dir=../../source_archive

clean:
	@rm -rf env source_archive/*.gz source_archive/*.xz pyqtdeploy-2.4* $(QT5SRC) $(HOME)/Android/Qt-$(QT5VER)
